from sklearn.model_selection import ParameterGrid
from model.main.rnn_model_v2 import RNN_Model as RNN
from utils.IOUtil import read_dataset_file
from utils.SettingPaper import rnn_paras as param_grid
# from utils.SettingPaper import rnn_paras_final as param_grid

def train_model(item):
    base_paras = {
    "data_name": data_name,
    "log_filename": all_model_file_name,
    "path_save_result": pathsave + requirement_variables[4],
    "filename": "RNN_sliding_{0}_hidden_size_{1}_layers_{2}_learning_rate_{3}_batch_size_{4}_epochs_{5}".format(item["sliding"], item["hidden_size"], item["n_layers"], item["learning_rate"], item["batch_size"], item["epochs"])
    }

    model_paras = {
    "hidden_size": item["hidden_size"],
    "sliding": item["sliding"],
    "n_layers": item["n_layers"],
    "learning_rate": item["learning_rate"],
    "batch_size": item["batch_size"],
    "epochs": item["epochs"]
    }

    model = RNN(paras = model_paras, other_paras = base_paras)
    model._run_()


test = ["eu", "wc", "cpu", "ram"]

for i in range(1):
    for item in test:
        if item == "eu":
            from utils.SettingPaper import traffic_eu as requirement_variables

        elif item == "uk":
            from utils.SettingPaper import traffic_uk as requirement_variables

        elif item == "wc":
            from utils.SettingPaper import worldcup as requirement_variables

        elif item == "cpu":
            from utils.SettingPaper import ggtrace_multi_cpu as requirement_variables

        elif item == "ram":
            from utils.SettingPaper import ggtrace_multi_ram as requirement_variables

        else:
            break
        print("-->%s iter %d:" %(item, i))

        pathsave = "paper/results/test/"
        all_model_file_name = "log_models"
        data_name = item

        # print(list(ParameterGrid(param_grid)))

        for itemx in list(ParameterGrid(param_grid)):
            train_model(itemx)
