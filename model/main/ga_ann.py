from model.optimizer import GA
from model.root_base import RootBase
from utils.MathUtil import elu, relu, tanh, sigmoid
import time

class Model(RootBase):
    def __init__(self, root_paras=None, net_paras=None, other_paras=None):
        RootBase.__init__(self, root_paras)
        self.dataset = other_paras["dataset"]
        self.train_valid_rate = other_paras["train_valid_rate"]

        self.hidden_size = net_paras["hidden_size"]
        self.activations = net_paras["activations"]
        self.epoch = net_paras["epoch"]
        self.pop_size = net_paras["pop_size"]
        self.pc = net_paras["pc"]
        self.pm = net_paras["pm"]
        self.domain_range = net_paras["domain_range"]

        self.filename = "GA_ANN-sliding_{}-net_para_{}".format(root_paras["sliding"], [self.hidden_size, self.activations,
                                                                                    self.epoch, self.pop_size, self.pc,
                                                                                    self.pm, self.train_valid_rate])
        if self.activations[0] == 0:
            self.activation1 = elu
        elif self.activations[0] == 1:
            self.activation1 = relu
        elif self.activations[0] == 2:
            self.activation1 = tanh
        else:
            self.activation1 = sigmoid

        if self.activations[1] == 0:
            self.activation2 = elu
        elif self.activations[1] == 1:
            self.activation2 = relu
        elif self.activations[1] == 2:
            self.activation2 = tanh
        else:
            self.activation2 = sigmoid

    def _train__(self):
        self.num_input = self.X_train.shape[1]
        self.num_output = self.y_train.shape[1]

        self.w1_size = self.num_input * self.hidden_size
        self.b1_size = self.hidden_size
        self.w2_size = self.hidden_size * self.num_output
        self.b2_size = self.num_output

        ga_paras = {
            "epoch": self.epoch, "pop_size": self.pop_size, "pc": self.pc, "pm": self.pm
        }
        root_paras = {
            "net_size": [self.num_input, self.hidden_size, self.num_output, self.w1_size, self.b1_size, self.w2_size,
                         self.b2_size],
            "X_train": self.X_train, "y_train": self.y_train,
            "X_valid": self.X_valid, "y_valid": self.y_valid,
            "activations": [self.activation1, self.activation2], "train_valid_rate": self.train_valid_rate,
            "print_train": self.print_train, "domain_range": self.domain_range
            }
        ga = GA.BaseGA(root_paras, ga_paras)
        self.model, self.loss_train = ga._train__()

    def _run__(self):
        self.time_system = time.time()
        self._preprocessing__(None, self.dataset)
        self.time_total_train = time.time()
        self._train__()
        self.time_total_train = round(time.time() - self.time_total_train, 4)
        self.time_epoch = round(self.time_total_train / self.epoch, 4)
        self.time_predict = time.time()
        y_actual, y_predict, y_actual_normalized, y_predict_normalized = self._forecasting__([self.activation1, self.activation2])
        self.time_predict = round(time.time() - self.time_predict, 6)
        self.time_system = round(time.time() - self.time_system, 4)
        # self._save_results__(y_actual, y_predict, y_actual_normalized, y_predict_normalized, self.loss_train)
        self._save_results_run_test__(y_actual, y_predict, y_actual_normalized, y_predict_normalized)
