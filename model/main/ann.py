from utils.PreprocessingUtil import MiniBatch
from model.root_base import RootBase
import time

class Model(RootBase):
    def __init__(self, root_paras=None, net_paras=None, other_paras=None):
        RootBase.__init__(self, root_paras)
        self.dataset = other_paras["dataset"]
        self.tf = other_paras["tf"]

        self.hidden_size = net_paras["hidden_size"]
        self.epoch = net_paras["epoch"]
        self.learning_rate = net_paras["learning_rate"]
        self.batch_size = net_paras["batch_size"]
        self.activations = net_paras["activations"]

        self.filename = "ANN-sliding_{}-net_para_{}".format(root_paras["sliding"], [self.hidden_size, self.epoch,
                            self.batch_size, self.learning_rate, self.activations, net_paras["optimizer_id"]])

        if self.activations[0] == 0:
            self.activation1 = self.tf.nn.elu
        elif self.activations[0] == 1:
            self.activation1 = self.tf.nn.relu
        elif self.activations[0] == 2:
            self.activation1 = self.tf.nn.tanh
        else:
            self.activation1 = self.tf.nn.sigmoid

        if self.activations[1] == 0:
            self.activation2 = self.tf.nn.elu
        elif self.activations[1] == 1:
            self.activation2 = self.tf.nn.relu
        elif self.activations[1] == 2:
            self.activation2 = self.tf.nn.tanh
        else:
            self.activation2 = self.tf.nn.sigmoid

        if net_paras["optimizer_id"] == 0:
            self.optimizer = self.tf.train.GradientDescentOptimizer
        elif net_paras["optimizer_id"] == 1:
            self.optimizer = self.tf.train.AdamOptimizer
        elif net_paras["optimizer_id"] == 2:
            self.optimizer = self.tf.train.AdagradOptimizer
        else:
            self.optimizer = self.tf.train.AdadeltaOptimizer

    # Create and train a tensorflow model of a neural network
    def _train__(self):
        # Reset the graph
        self.tf.reset_default_graph()

        X_size = self.X_train.shape[1]
        h_size = self.hidden_size
        y_size = self.y_train.shape[1]

        # Placeholders for input and output data
        X = self.tf.placeholder("float64", shape=[None, X_size], name='X')
        y = self.tf.placeholder("float64", shape=[None, y_size], name='y')

        # now declare the weights connecting the input to the hidden layer
        W1 = self.tf.Variable(self.tf.random_normal([X_size, h_size], stddev=0.03, dtype=self.tf.float64), name="W1")
        b1 = self.tf.Variable(self.tf.random_normal([h_size], dtype=self.tf.float64), name="b1")
        # and the weights connecting the hidden layer to the output layer
        W2 = self.tf.Variable(self.tf.random_normal([h_size, y_size], stddev=0.03, dtype=self.tf.float64), name='W2')
        b2 = self.tf.Variable(self.tf.random_normal([y_size], dtype=self.tf.float64), name='b2')

        # calculate the output of the hidden layer
        hidden_out = self.activation1(self.tf.add(self.tf.matmul(X, W1), b1))
        # Forward propagation # now calculate the hidden layer output
        y_ = self.activation2(self.tf.add(self.tf.matmul(hidden_out, W2), b2))

        # Loss function
        loss = self.tf.reduce_mean( self.tf.abs(y_ - y) )

        # Backward propagation
        opt = self.optimizer(learning_rate=self.learning_rate)
        train = opt.minimize(loss)

        # Initialize variables and run session
        init = self.tf.global_variables_initializer()
        sess = self.tf.Session()
        sess.run(init)

        # Go through num_iters iterations
        weights1, bias1, weights2, bias2= None, None, None, None

        seed = 0
        for epoch in range(self.epoch):
            seed += 1

            mini_batches = MiniBatch(self.X_train, self.y_train, self.batch_size).random_mini_batches(seed=seed)

            for mini_batch in mini_batches:
                X_batch, y_batch = mini_batch

                X_batch = X_batch.T
                y_batch = y_batch.T

                sess.run(train, feed_dict={X: X_batch, y: y_batch})

            loss_epoch, weights1, bias1, weights2, bias2 = sess.run([loss, W1, b1, W2, b2], feed_dict={X: self.X_train, y: self.y_train})
            self.loss_train.append(loss_epoch)
            if self.print_train:
                print("Epoch: {}".format(epoch + 1), "loss = {}".format(loss_epoch))

        self.model = {"w1": weights1, "b1": bias1,"w2": weights2, "b2": bias2}
        sess.close()
        #print("Build model and train done!!!")


    def _forecasting__(self, activations=None, X_data=None):
        # Evaluate models on the test set
        X_size = self.X_test.shape[1]
        y_size = self.y_test.shape[1]

        X = self.tf.placeholder("float64", shape=[None, X_size], name='X')
        y = self.tf.placeholder("float64", shape=[None, y_size], name='y')

        W1 = self.tf.Variable(self.model["w1"])
        b1 = self.tf.Variable(self.model["b1"])
        W2 = self.tf.Variable(self.model["w2"])
        b2 = self.tf.Variable(self.model["b2"])

        hidden_out = self.activation1(self.tf.add(self.tf.matmul(X, W1), b1))
        y_ = self.activation2(self.tf.add(self.tf.matmul(hidden_out, W2), b2))

        # Calculate the predicted outputs
        init = self.tf.global_variables_initializer()
        with self.tf.Session() as sess:
            sess.run(init)
            y_est_np = sess.run(y_, feed_dict={X: self.X_test, y: self.y_test})

            pred_inverse = self.scaler.inverse_transform(y_est_np)
            real_inverse = self.scaler.inverse_transform(self.y_test)
        return real_inverse, pred_inverse, self.y_test, y_est_np


    def _run__(self):
        self.time_system = time.time()
        self._preprocessing__(None, self.dataset)
        self.time_total_train = time.time()
        self._train__()
        self.time_total_train = round(time.time() - self.time_total_train, 4)
        self.time_epoch = round(self.time_total_train / self.epoch, 4)
        self.time_predict = time.time()
        y_actual, y_predict, y_actual_normalized, y_predict_normalized = self._forecasting__()
        self.time_predict = round(time.time() - self.time_predict, 6)
        self.time_system = round(time.time() - self.time_system, 4)
        #self._save_results__(y_actual, y_predict, y_actual_normalized, y_predict_normalized, self.loss_train)
        self._save_results_run_test__(y_actual, y_predict, y_actual_normalized, y_predict_normalized)

