### LIBRARIES AND SETTING
import numpy as np
import pandas as pd
import math
import sklearn
import sklearn.preprocessing
import time
import os
# import matplotlib.pyplot as plt
import torch
import torch.nn as nn
from torch.autograd import Variable
from sklearn.metrics import mean_absolute_error, mean_squared_error
from model.root_base import RootBase
# from model.main import rnn
import torch.optim as optim


class RNN_Model(RootBase):
    def __init__(self, paras = None, other_paras = None):
        self.data_name = other_paras["data_name"]
        self.hidden_size = paras["hidden_size"]
        self.sliding = paras["sliding"]
        self.n_inputs = None
        self.n_outputs = 1
        self.n_layers = paras["n_layers"]
        self.learning_rate = paras["learning_rate"]
        self.batch_size = paras["batch_size"]
        self.epochs = paras["epochs"]
        self.test_set_size_percentage = None
        self.df = None
        self.data = None
        self.output_idx = None
        self.output_multi = 0
        self.draw = 1
        self.print_train = 0

        self.index_in_epoch = 0
        self.perm_array = None
        self.filename = other_paras["filename"]
        self.path_save_result = other_paras["path_save_result"]
        self.log_filename = other_paras["log_filename"]
        self.time_total_train, self.time_epoch, self.time_predict, self.time_system = None, None, None, None

    def _import_data_(self):
        if self.data_name == 'eu':
            i_df = pd.read_csv('./data/traffic/internet_traffic_eu_5m.csv', usecols = [1]).values.astype(float)
            df = i_df[:14750]
            self.n_inputs = 1
            self.test_set_size_percentage = 16
            self.output_idx = 0
        if self.data_name == 'wc':
            i_df = pd.read_csv('./data/worldcup/worldcup98_5m.csv', usecols = [1]).values.astype(float)
            df = i_df[-7560:]
            self.n_inputs = 1
            self.test_set_size_percentage = 15
            self.output_idx = 0
        if self.data_name == 'ram' or self.data_name == 'cpu':
            i_df = pd.read_csv('./data/google/google_5m.csv', usecols = [1,2]).values.astype(float)
            df = i_df[:8300]
            self.n_inputs = 2
            self.test_set_size_percentage = 15
            if self.data_name == "ram":
                self.output_idx = 1
            elif self.data_name == "cpu":
                self.output_idx = 0
        # self.df = df.values.reshape(-1, self.n_inputs)
        self.df = df
        # Canculate change data
        self.data = df[1:, :] - df[:-1, :]

    def _normalize_data_(self, data):
        min_max_scaler = sklearn.preprocessing.MinMaxScaler()
        data = min_max_scaler.fit_transform(data)
        return data

    def _re_scale_(self, res, mi, mx):
        return res*(mx - mi) + mi

    def _load_data_single_(self, stock, seq_len):
        data_raw = stock # convert to numpy array
        data = []

        # create all possible sequences of length seq_len
        for index in range(len(data_raw) - seq_len):
            data.append(data_raw[index: index + seq_len])

        data = np.array(data)
        test_set_size = int(np.round(self.test_set_size_percentage/100*data.shape[0]))
        train_set_size = data.shape[0] - test_set_size

        x_train = torch.Tensor(data[:train_set_size, :-1, :])
        y_train = torch.Tensor(data[:train_set_size, 1:, :])

        x_test = torch.Tensor(data[train_set_size:, :-1, :])
        y_test = torch.Tensor(data[train_set_size:, 1:, :])

        return [x_train, y_train, x_test, y_test]

    def _load_data_multi_(self, stock, seq_len):
        data_raw_input = stock # convert to numpy array
        data_raw_output = stock[:, self.output_idx].reshape(len(stock), 1)
        data_input = []
        data_output = []

        # create all possible sequences of length seq_len
        for index in range(len(data_raw_input) - seq_len):
            data_input.append(data_raw_input[index: index + seq_len])
            data_output.append(data_raw_output[index: index + seq_len])

        data_input = np.array(data_input)
        data_output = np.array(data_output)
        test_set_size = int(np.round(self.test_set_size_percentage/100*data_input.shape[0]))
        train_set_size = data_input.shape[0] - test_set_size

        x_train = torch.Tensor(data_input[:train_set_size, :-1, :])
        y_train = torch.Tensor(data_output[:train_set_size, 1:, :])

        x_test = torch.Tensor(data_input[train_set_size:, :-1, :])
        y_test = torch.Tensor(data_output[train_set_size:, 1:, :])

        return [x_train, y_train, x_test, y_test]

    def _get_next_batch_(self, normalized_x_train, normalized_y_train):
        start = self.index_in_epoch
        self.index_in_epoch += self.batch_size

        if self.index_in_epoch > normalized_x_train.shape[0]:
            np.random.shuffle(self.perm_array) # shuffle permutation array
            start = 0 # start next epoch
            self.index_in_epoch = self.batch_size

        end = self.index_in_epoch
        return normalized_x_train[self.perm_array[start:end]], normalized_y_train[self.perm_array[start:end]]

    def _import_model_(self):
        model = RNN(self.batch_size, self.n_inputs, self.hidden_size, self.n_outputs, self.n_layers)
        return model


    def _run_(self):
        system_time_start = time.clock()
        self._import_data_()

        # Min-max use for reverse MinMaxScaler
        if self.n_inputs == 1:
            val_max = self.data.max()
            val_min = self.data.min()
        else:
            val_max = self.data[:, self.output_idx].max()
            val_min = self.data[:, self.output_idx].min()

        # MinMaxScaler
        data_norm = self._normalize_data_(self.data.copy())
        seq_len = self.sliding + 1

        # Get normalized data
        if self.n_inputs == 1:
            normalized_x_train, normalized_y_train, normalized_x_test, normalized_y_test = self._load_data_single_(data_norm, seq_len)
        else:
            normalized_x_train, normalized_y_train, normalized_x_test, normalized_y_test = self._load_data_multi_(data_norm, seq_len)

        self.perm_array = np.arange(normalized_x_train.shape[0])
        np.random.shuffle(self.perm_array)

        train_set_size = normalized_x_train.shape[0]
        test_set_size = normalized_x_test.shape[0]

        ## Setting RNN
        model = self._import_model_()
        criterion = nn.L1Loss()
        optimizer = optim.SGD(model.parameters(), lr = self.learning_rate)

        ## Train model
        loss_vals = []
        train_time_start = time.clock()
        for iter in range(int(self.epochs*train_set_size/self.batch_size)):
            x_batch, y_batch = self._get_next_batch_(normalized_x_train, normalized_y_train) # fetch the next training batch
            predict = model(x_batch)
            optimizer.zero_grad()
            loss = criterion(predict[:, :, :], y_batch)
            loss.backward()
            optimizer.step()
            if iter % int(train_set_size/self.batch_size) == 0:
                train_pred = model(normalized_x_train)
                loss = criterion(train_pred[:, :, :], normalized_y_train)
                loss_vals.append(loss.data[0])
                print("epoch:", int(iter/(train_set_size/self.batch_size)), "loss:", loss.data[0])

        # Calculate train time and epochs time
        self.time_total_train = (time.clock() - train_time_start)
        self.time_epoch = self.time_total_train/self.epochs
        # Get loss
        loss_vals = torch.stack(loss_vals).detach().numpy()
        # predict
        pred_time_start = time.clock()
        normalized_train_pred = model(normalized_x_train)
        normalized_test_pred = model(normalized_x_test)

        self.time_predict = (time.clock() - pred_time_start)/(normalized_train_pred.size(0) + normalized_test_pred.size(0))
        # reverse MinMaxScaler
        train_pred = self._re_scale_(normalized_train_pred, val_min, val_max).detach().numpy()
        test_pred = self._re_scale_(normalized_test_pred, val_min, val_max).detach().numpy()

        if self.n_inputs == 1:
            _, y_train, _, y_test = self._load_data_single_(self.data, seq_len)
            _, original_y_train, _, original_y_test = self._load_data_single_(self.df, seq_len)
            # _, _, _, original_normalized_y_test = self._load_data_single_(self._normalize_data_(self.df), seq_len)
        else:
            _, y_train, _, y_test = self._load_data_multi_(self.data, seq_len)
            _, original_y_train, _, original_y_test = self._load_data_multi_(self.df, seq_len)
            # _, _, _, original_normalized_y_test = self._load_data_multi_(self._normalize_data_(self.df), seq_len)

        #-----------------------------------------------------------------------
        self.time_system = time.clock() - system_time_start
        #----------------------------------------------------------------------\
        # Reconstruct predicted values
        test_pred = test_pred + original_y_test[:-1]
        # normalized_test_pred = normalized_test_pred + original_normalized_y_test[:-1]

        # self._save_results_run_test__(y_test[:, -1].numpy(), test_pred[:, -1], normalized_y_test[:, -1].detach().numpy() ,normalized_test_pred[:, -1].detach().numpy())
        self._save_results__(original_y_test[1:, -1].numpy(), test_pred[:, -1].numpy(), normalized_y_test[:, -1].detach().numpy() ,normalized_test_pred[:, -1].detach().numpy(), loss_vals)


class LSTM_Model(RNN_Model):
    def __init__(self, paras, other_paras):
        RNN_Model.__init__(self, paras, other_paras)
    def _import_model_(self):
        model = LSTM(self.batch_size, self.n_inputs, self.hidden_size, self.n_outputs, self.n_layers)
        return model


class RNN(nn.Module):
    def __init__(self, batch_size, n_inputs, n_neurons, n_outputs, n_layers):
        super(RNN, self).__init__()

        self.n_neurons = n_neurons
        self.batch_size = batch_size
        self.n_inputs = n_inputs
        self.n_outputs = n_outputs
        self.n_layers = n_layers

        self.rnn = nn.RNN(self.n_inputs, self.n_neurons, self.n_layers, batch_first = True)
        self.FC = nn.Linear(self.n_neurons, self.n_outputs)

    def forward(self, X):
        self.hidden = torch.zeros(self.n_layers, X.shape[0], self.n_neurons)

        out, self.hidden = self.rnn(X, self.hidden)
        predict = self.FC(out)
        return predict

class LSTM(nn.Module):
    def __init__(self, batch_size, n_inputs, n_neurons, n_outputs, n_layers):
        super(LSTM, self).__init__()

        self.n_neurons = n_neurons
        self.batch_size = batch_size
        self.n_inputs = n_inputs
        self.n_outputs = n_outputs
        self.n_layers = n_layers

        self.lstm = nn.LSTM(self.n_inputs, self.n_neurons, self.n_layers, batch_first = True)
        self.FC = nn.Linear(self.n_neurons, self.n_outputs)

    def forward(self, X):
        self.hidden = (torch.zeros(self.n_layers, X.shape[0], self.n_neurons), \
                torch.zeros(self.n_layers, X.shape[0], self.n_neurons))

        out, self.hidden = self.lstm(X, self.hidden)
        predict = self.FC(out)
        return predict
