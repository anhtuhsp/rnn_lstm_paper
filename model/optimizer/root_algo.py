import numpy as np
from copy import deepcopy
from sklearn.metrics import mean_absolute_error

class RootAlgo(object):
    """
    This is root of all Natural Inspired Algorithms
    """
    ID_MIN_PROBLEM = 0
    ID_MAX_PROBLEM = -1

    def __init__(self, root_paras = None):
        self.X_train = root_paras["X_train"]
        self.y_train = root_paras["y_train"]
        self.X_valid = root_paras["X_valid"]
        self.y_valid = root_paras["y_valid"]
        self.activations = root_paras["activations"]
        self.train_valid_rate = root_paras["train_valid_rate"]
        self.print_train = root_paras["print_train"]
        self.domain_range = root_paras["domain_range"]
        self.net_size = root_paras["net_size"]
        # [self.input_size, self.hidden_size, self.output_size, self.w1_size, self.b1_size, self.w2_size, self.b2_size]
        self.input_size, self.hidden_size, self.output_size = self.net_size[0], self.net_size[1], self.net_size[2]
        self.w1_size, self.b1_size, self.w2_size, self.b2_size = self.net_size[3], self.net_size[4], self.net_size[5], \
                                                                 self.net_size[6]
        self.problem_size = sum(self.net_size[3:])
        self.model, self.loss_train = None, []

    def _get_model__(self, indiv=None):
        w1 = np.reshape(indiv[:self.w1_size], (self.input_size, self.hidden_size))
        b1 = np.reshape(indiv[self.w1_size:self.w1_size + self.b1_size], (-1, self.hidden_size))
        w2 = np.reshape(indiv[self.w1_size + self.b1_size: self.w1_size + self.b1_size + self.w2_size],(self.hidden_size, self.output_size))
        b2 = np.reshape(indiv[self.w1_size + self.b1_size + self.w2_size:], (-1, self.output_size))
        return {"w1": w1, "b1": b1, "w2": w2, "b2": b2}

    def _create_solution__(self):
        solution = np.random.uniform(self.domain_range[0], self.domain_range[1], self.problem_size)
        fitness = self._fitness_model__(model=solution, minmax=0)
        return [solution, fitness]

    def _get_average_mae__(self, individual=None, X_data=None, y_data=None):
        w1 = np.reshape(individual[:self.w1_size], (self.input_size, self.hidden_size))
        b1 = np.reshape(individual[self.w1_size:self.w1_size + self.b1_size], (-1, self.hidden_size))
        w2 = np.reshape(individual[self.w1_size + self.b1_size: self.w1_size + self.b1_size + self.w2_size],
                        (self.hidden_size, self.output_size))
        b2 = np.reshape(individual[self.w1_size + self.b1_size + self.w2_size:], (-1, self.output_size))
        hidd = self.activations[0](np.add(np.matmul(X_data, w1), b1))
        y_pred = self.activations[1](np.add(np.matmul(hidd, w2), b2))
        return mean_absolute_error(y_pred, y_data)

    def _fitness_model__(self, model=None, minmax=0):
        """ distance between the sum of an individual numbers and the target number. Lower is better"""
        averageTrainMAE = self._get_average_mae__(model, self.X_train, self.y_train)
        averageValidationMAE = self._get_average_mae__(model, self.X_valid, self.y_valid)
        if minmax == 0:
            return (self.train_valid_rate[0] * averageTrainMAE + self.train_valid_rate[1] * averageValidationMAE)
        else:
            return 1.0 / (self.train_valid_rate[0] * averageTrainMAE + self.train_valid_rate[1] * averageValidationMAE)

    def _fitness_encoded__(self, encoded, id_pos):
        return self._fitness_model__(encoded[id_pos], minmax=0)


    def _get_global_best__(self, pop=None, id_fitness=None, id_best=None):
        sorted_pop = sorted(pop, key=lambda temp: temp[id_fitness])
        return deepcopy(sorted_pop[id_best])

    def _train__(self):
        pass

