###### Config for test

###: Variables
ggtrace_multi_cpu = [
    "data/google/",    # pd.readfilepath
    [3, 4],        # usecols trong pd
    False,      # output_multi
    0,       # output_index
    "multi_cpu/",     # path_save_result
]

ggtrace_multi_ram = [
    "data/google/",    # pd.readfilepath
    [3, 4],        # usecols trong pd
    False,      # output_multi
    1,       # output_index
    "multi_ram/",     # path_save_result
]


traffic_eu = [
    "data/traffic/",    # pd.readfilepath
    [1],        # usecols trong pd
    False,      # output_multi
    None,       # output_index
    "eu/",     # path_save_result
]

traffic_uk = [
    "data/traffic/",    # pd.readfilepath
    [1],        # usecols trong pd
    False,      # output_multi
    None,       # output_index
    "uk/",     # path_save_result
]

worldcup = [
    "data/worldcup/",    # pd.readfilepath
    [0],        # usecols trong pd
    False,      # output_multi
    None,       # output_index
    "worldcup/",     # path_save_result
]


####: ANN
ann_paras = {
    "sliding_window": [2, 3, 4, 5],
    "hidden_size" : [8, 12, 15],
    "activations": [(0, 0)],  # 0: elu, 1:relu, 2:tanh, 3:sigmoid
    "optimizer_id": [0],   # GradientDescentOptimizer, AdamOptimizer, AdagradOptimizer, AdadeltaOptimizer
    "learning_rate": [0.001, 0.005, 0.01],
    "epoch": [5000, 10000],
    "batch_size": [64],
}


#### : GA-ANN
ga_ann_paras = {
    "sliding_window": [2, 3, 4, 5],
    "hidden_size" : [8, 12, 15],
    "activations": [(0, 0)],             # 0: elu, 1:relu, 2:tanh, 3:sigmoid
    "train_valid_rate": [(0.6, 0.4)],

    "epoch": [650, 800],
    "pop_size": [250],                  # 100 -> 900
    "pc": [0.95],                       # 0.85 -> 0.97
    "pm": [0.025],                      # 0.005 -> 0.10
    "domain_range": [(-1, 1)]           # lower and upper bound
}

###: CRO
cro_ann_paras = {
    "sliding_window": [2, 3, 4, 5],
    "hidden_size" : [8, 12, 15],
    "activations": [(0, 0)],             # 0: elu, 1:relu, 2:tanh, 3:sigmoid
    "train_valid_rate": [(0.6, 0.4)],

    "epoch": [650, 800],
    "pop_size": [250],
    "G": [(0.2, 0.02)],
    "GCR": [0.1],
    "po": [0.4],
    "Fb": [0.7, 0.9],
    "Fa": [0.1, 0.2],
    "Fd": [0.1],
    "Pd": [0.1],
    "k": [3],
    "domain_range": [(-1, 1)]           # lower and upper bound
}

###: OCRO
ocro_ann_paras = {
    "sliding_window": [2, 3, 4, 5],
    "hidden_size" : [8, 12, 15],
    "activations": [(0, 0)],             # 0: elu, 1:relu, 2:tanh, 3:sigmoid
    "train_valid_rate": [(0.6, 0.4)],

    "epoch": [600, 700, 800],
    "pop_size": [250, 350],
    "G": [(0.2, 0.02)],
    "restart_count": [15, 25, 35],
    "GCR": [0.1, 0.2],
    "po": [0.4],
    "Fb": [0.6, 0.7, 0.8],
    "Fa": [0.1],
    "Fd": [0.2, 0.3, 0.4, 0.5],
    "Pd": [0.1],
    "k": [3],
    "domain_range": [(-1, 1)]           # lower and upper bound
}



######################## Paras according to the paper

####: ANN
ann_paras_final = {
    "sliding_window": [2, 5],
    "hidden_size" : [10],
    "activations": [(0, 0)],  # 0: elu, 1:relu, 2:tanh, 3:sigmoid
    "optimizer_id": [0],   # GradientDescentOptimizer, AdamOptimizer, AdagradOptimizer, AdadeltaOptimizer
    "learning_rate": [0.0001],
    "epoch": [10000],
    "batch_size": [64],
}

#### : GA-ANN
ga_ann_paras_final = {
    "sliding_window": [2, 5],
    "hidden_size" : [10],
    "activations": [(0, 0)],             # 0: elu, 1:relu, 2:tanh, 3:sigmoid
    "train_valid_rate": [(0.6, 0.4)],

    "epoch": [700],
    "pop_size": [250],                  # 100 -> 900
    "pc": [0.95],                       # 0.85 -> 0.97
    "pm": [0.025],                      # 0.005 -> 0.10
    "domain_range": [(-1, 1)]           # lower and upper bound
}

#### : CRO-ANN
cro_ann_paras_final = {
    "sliding_window": [2, 5],
    "hidden_size" : [10],
    "activations": [(0, 0)],             # 0: elu, 1:relu, 2:tanh, 3:sigmoid
    "train_valid_rate": [(0.6, 0.4)],

    "epoch": [1000],
    "pop_size": [250],
    "G": [[0.02, 0.2]],
    "GCR": [0.1],
    "po": [0.4],
    "Fb": [0.9],
    "Fa": [0.1],
    "Fd": [0.1],
    "Pd": [0.1],
    "k": [3],
    "domain_range": [(-1, 1)]           # lower and upper bound
}

#### : OCRO-ANN
ocro_ann_paras_final = {
    "sliding_window": [2, 5],
    "hidden_size" : [10],
    "activations": [(0, 0)],             # 0: elu, 1:relu, 2:tanh, 3:sigmoid
    "train_valid_rate": [(0.6, 0.4)],

    "epoch": [1000],
    "pop_size": [250],
    "G": [[0.02, 0.2]],
    "restart_count": [55],
    "GCR": [0.1],
    "po": [0.4],
    "Fb": [0.8],
    "Fa": [0.1],
    "Fd": [0.3],
    "Pd": [0.1],
    "k": [3],
    "domain_range": [(-1, 1)]           # lower and upper bound
}

### RNN
rnn_paras = {
"hidden_size": [10],
"sliding": [2],
"n_layers": [1],
"learning_rate": [0.0005],
"batch_size": [64],
"epochs": [1000]
}

lstm_paras = {
"hidden_size": [10],
"sliding": [2, 5],
"n_layers": [1],
"learning_rate": [0.003],
"batch_size": [64],
"epochs": [1000]
}
