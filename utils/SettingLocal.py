###### Config for test

###: Variables
ggtrace_multi_cpu = [
    "data/google/",    # pd.readfilepath
    [3, 4],        # usecols trong pd
    False,      # output_multi
    0,       # output_index
    "multi_cpu/",     # path_save_result
]

ggtrace_multi_ram = [
    "data/google/",    # pd.readfilepath
    [3, 4],        # usecols trong pd
    False,      # output_multi
    1,       # output_index
    "multi_ram/",     # path_save_result
]


traffic_eu = [
    "data/traffic/",    # pd.readfilepath
    [1],        # usecols trong pd
    False,      # output_multi
    None,       # output_index
    "eu/",     # path_save_result
]

traffic_uk = [
    "data/traffic/",    # pd.readfilepath
    [1],        # usecols trong pd
    False,      # output_multi
    None,       # output_index
    "uk/",     # path_save_result
]

worldcup = [
    "data/worldcup/",    # pd.readfilepath
    [0],        # usecols trong pd
    False,      # output_multi
    None,       # output_index
    "worldcup/",     # path_save_result
]



####: ANN
ann_paras = {
    "sliding_window": [3],
    "hidden_size" : [12],
    "activations": [(0, 0)],  # 0: elu, 1:relu, 2:tanh, 3:sigmoid
    "optimizer_id": [0],   # GradientDescentOptimizer, AdamOptimizer, AdagradOptimizer, AdadeltaOptimizer
    "learning_rate": [0.001],
    "epoch": [10],
    "batch_size": [64],
}


#### : GA-ANN
ga_ann_paras = {
    "sliding_window": [3],
    "hidden_size" : [12],
    "activations": [(0, 0)],             # 0: elu, 1:relu, 2:tanh, 3:sigmoid
    "train_valid_rate": [(0.6, 0.4)],

    "epoch": [10],
    "pop_size": [10],                  # 100 -> 900
    "pc": [0.95],                       # 0.85 -> 0.97
    "pm": [0.025],                      # 0.005 -> 0.10
    "domain_range": [(-1, 1)]           # lower and upper bound
}


#### : CRO-ANN
cro_ann_paras = {
    "sliding_window": [3],
    "hidden_size" : [12],
    "activations": [(0, 0)],             # 0: elu, 1:relu, 2:tanh, 3:sigmoid
    "train_valid_rate": [(0.6, 0.4)],

    "epoch": [10],
    "pop_size": [10],
    "GCR": [0.1],
    "po": [0.4],
    "Fb": [0.6],
    "Fa": [0.1],
    "Fd": [0.3],
    "Pd": [0.1],
    "k": [3],
    "G": [(0.2, 0.02)],
    "domain_range": [(-1, 1)]           # lower and upper bound
}

### : OCRO-ANN
ocro_ann_paras = {
    "sliding_window": [3],
    "hidden_size" : [12],
    "activations": [(0, 0)],             # 0: elu, 1:relu, 2:tanh, 3:sigmoid
    "train_valid_rate": [(0.6, 0.4)],

    "epoch": [10],
    "pop_size": [10],
    "G": [(0.1, 0.02)],
    "restart_count": [25],
    "GCR": [0.1],
    "po": [0.4],
    "Fb": [0.6],
    "Fa": [0.1],
    "Fd": [0.3],
    "Pd": [0.1],
    "k": [3],
    "domain_range": [(-1, 1)]           # lower and upper bound
}

