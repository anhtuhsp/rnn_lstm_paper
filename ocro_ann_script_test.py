from sklearn.model_selection import ParameterGrid
from model.main.ocro_ann import Model as OCROANN
from utils.IOUtil import read_dataset_file
from utils.SettingPaper import ocro_ann_paras_final as param_grid

def train_model(item):
    base_paras = {
        "output_multi": requirement_variables[2],
        "output_index": requirement_variables[3],
        "method_statistic": 0,  # 0: sliding window, 1: mean, 2: min-mean-max, 3: min-median-max
        "log_filename": all_model_file_name,
        "path_save_result": pathsave + requirement_variables[4],
        "draw": True,
        "print_train": False,
        "sliding": item["sliding_window"],
        "dataset_index": idx
    }
    net_paras = {
        "hidden_size": item["hidden_size"], "activations": item["activations"], "epoch": item["epoch"],
        "pop_size": item["pop_size"], "po": item["po"], "Fb": item["Fb"], "Fa": item["Fa"], "Fd": item["Fd"],
        "Pd": item["Pd"], "G": item["G"], "GCR": item["GCR"], "k": item["k"],
        "restart_count": item["restart_count"], "domain_range": item["domain_range"]
    }
    other_paras = {
        "dataset": dataset, "train_valid_rate": item["train_valid_rate"]
    }

    p = OCROANN(root_paras=base_paras, net_paras=net_paras, other_paras=other_paras)
    p._run__()

test = ["eu", "uk", "wc", "cpu", "ram"]

for i in range(15):
    for item in test:
        if item == "eu":
            from utils.SettingPaper import traffic_eu as requirement_variables
            filename = requirement_variables[0] + 'internet-traffic-data-in-bits-fr_EU_5m.csv'
            idx = (10030, 1, 14750)  # 68 / 16 / 16

        elif item == "uk":
            from utils.SettingPaper import traffic_uk as requirement_variables
            filename = requirement_variables[0] + 'internet-traffic-data-in-bits-fr_UK_5m.csv'
            idx = (10030, 1, 14750)  # 68 / 16 / 16

        elif item == "wc":
            from utils.SettingPaper import worldcup as requirement_variables
            filename = requirement_variables[0] + 'wc98_workload_5min_formatted.csv'
            idx = (5292, 1, 7560)  # 70 / 15 / 15

        elif item == "cpu":
            from utils.SettingPaper import ggtrace_multi_cpu as requirement_variables
            filename = requirement_variables[0] + 'data_resource_usage_5Minutes_6176858948_formatted.csv'
            idx = (5810, 1, 8300)  # 70 / 15 / 15

        elif item == "ram":
            from utils.SettingPaper import ggtrace_multi_ram as requirement_variables
            filename = requirement_variables[0] + 'data_resource_usage_5Minutes_6176858948_formatted.csv'
            idx = (5810, 1, 8300)  # 70 / 15 / 15

        else:
            break

        # pathsave = "paper/results/final/"             # test 1
        # all_model_file_name = "log_models"
        pathsave = "paper/results/stability/"           # test 2
        all_model_file_name = "stability_ocro_ann"
        dataset = read_dataset_file(filename, requirement_variables[1])
        # Create combination of params.
        for item in list(ParameterGrid(param_grid)):
            train_model(item)
