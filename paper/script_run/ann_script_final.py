import tensorflow as tf
from sklearn.model_selection import ParameterGrid
from model.main.ann import Model as ANN
from utils.IOUtil import read_dataset_file
from utils.SettingPaper import ann_paras as param_grid

def train_model(item):
    base_paras = {
        "output_multi": requirement_variables[2],
        "output_index": requirement_variables[3],
        "method_statistic": 0,  # 0: sliding window, 1: mean, 2: min-mean-max, 3: min-median-max
        "log_filename": "log_models",
        "path_save_result": pathsave + requirement_variables[4],
        "draw": True,
        "print_train": False,
        "sliding": item["sliding_window"],
        "dataset_index": idx
    }
    net_paras = {
        "hidden_size": item["hidden_size"], "epoch": item["epoch"], "batch_size": item["batch_size"],
        "learning_rate": item["learning_rate"], "activations": item["activations"], "optimizer_id": item["optimizer_id"]
    }
    other_paras = {
        "dataset": dataset, "tf": tf
    }

    p = ANN(root_paras=base_paras, net_paras=net_paras, other_paras=other_paras)
    p._run__()

test = ["eu", "uk", "wc", "cpu", "ram"]

for item in test:
    if item == "eu":
        from utils.SettingPaper import traffic_eu as requirement_variables
        filename = requirement_variables[0] + 'internet-traffic-data-in-bits-fr_EU_5m.csv'
        idx = (11800, 0, 14750)

    elif item == "uk":
        from utils.SettingPaper import traffic_uk as requirement_variables
        filename = requirement_variables[0] + 'internet-traffic-data-in-bits-fr_UK_5m.csv'
        idx = (11800, 0, 14750)

    elif item == "wc":
        from utils.SettingPaper import worldcup as requirement_variables
        filename = requirement_variables[0] + 'wc98_workload_5min_formatted.csv'
        idx = (6048, 0, 7560)

    elif item == "cpu":
        from utils.SettingPaper import ggtrace_multi_cpu as requirement_variables
        filename = requirement_variables[0] + 'data_resource_usage_5Minutes_6176858948_formatted.csv'
        idx = (6640, 0, 8300)

    elif item == "ram":
        from utils.SettingPaper import ggtrace_multi_ram as requirement_variables
        filename = requirement_variables[0] + 'data_resource_usage_5Minutes_6176858948_formatted.csv'
        idx = (6640, 0, 8300)

    else:
        break

    pathsave = "paper/results/ann/"
    dataset = read_dataset_file(filename, requirement_variables[1])

    # Create combination of params.
    for item in list(ParameterGrid(param_grid)):
        train_model(item)
