## Outline

Abstract
1. Intro
2. Related Word
3. Model Architecture
4. Experiments
5. Conclusion

## Content
#### Diverse models
```
ANN
GA + ANN 
CRO + ANN
RNN 
LSTM
```
#### Evaluation
```
Accuracy: RMSE, MAE, MAPE
Speed: Training Time (Time / epoch), Predicting Time, System Time
```
#### Diverse datasets
```
Worldcup (5m)
Traffic (5m - EU, UK)
Google Trace (5m - multivariate)
```
