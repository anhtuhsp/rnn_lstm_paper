import numpy as np
import time
from copy import deepcopy

class Base(object):
    ID_REEF_AFTER_SORTED = 0       # High fitness choice : -1,  when low fitness choice : 0
    ID_SOL = 0
    ID_FIT = 1
    """
    Link:
        http://downloads.hindawi.com/journals/tswj/2014/739768.pdf
    """

    def __init__(self, cro_para = None):
        self.epoch = cro_para["epoch"]
        self.pop_size = cro_para["pop_size"]            # ~ number of space
        self.po = cro_para["po"]
        self.Fb = cro_para["Fb"]
        self.Fa = cro_para["Fa"]
        self.Fd = cro_para["Fd"]
        self.Pd = cro_para["Pd"]
        self.Pd_thres = cro_para["Pd"]
        self.k = cro_para["k"]
        self.alpha = 10 * cro_para["Pd"] / self.epoch
        self.domain_range = cro_para["domain_range"]
        self.problem_size = cro_para["problem_size"]

        self.pop = [None for _ in range(self.pop_size)]
        self.num_occupied = None
        self.larvaes = []
        self.loss_train = []

    def fitness_reef(self, reef=None):
        return sum([x*x for x in reef])

    def fitness_encoded(self, encoded=None):
        return self.fitness_reef(encoded[Base.ID_SOL])

    def create_population(self):
        self.num_occupied = int(self.pop_size / (1 + self.po))
        id_occupied = np.random.choice(range(self.pop_size), self.num_occupied, replace=False)
        for i in range(len(id_occupied)):
            self.pop[id_occupied[i]] = self.create_reef()

    def create_reef(self):
        reef = np.random.uniform(self.domain_range[0], self.domain_range[1], self.problem_size)
        fitness = self.fitness_reef(reef)
        return [reef, fitness]

    # Thuc hien Broadcast Spawning and Brooding
    def broadcast_spawning_and_brooding(self):
        ids_occupied = np.where(np.array(self.pop) != None)[0]
        ids_selected = np.random.choice(list(ids_occupied), int(self.num_occupied * self.Fb), replace=False)
        ids_unselected = ids_occupied[~np.in1d(ids_occupied, ids_selected)]
        #ids_unselected = np.setdiff1d(ids_occupied, ids_selected)


        ## Broadcast Spawning
        run_time = int(len(ids_selected) / 2)
        for i in range(run_time):
            p1, p2 = np.random.choice(ids_selected, 2, replace=False)
            larvae = self.crossover_one_child_multi_point(self.pop[p1][Base.ID_SOL], self.pop[p2][Base.ID_SOL])
            self.larvaes.append(larvae)
            ids_selected = np.delete(ids_selected, [np.argwhere(ids_selected == p1), np.argwhere(ids_selected == p2)])

        ## Brooding
        for idx in ids_unselected:
            temp = self.mutation_flip_point(self.pop[idx][Base.ID_SOL])
            self.larvaes.append(temp)

        # Thuc hien qua trinh tiep dat doi voi cac Larva
        for larvae in self.larvaes:
            larvae_fit = self.fitness_reef(larvae)
            for i in range(self.k):
                pos = np.random.randint(0, self.pop_size)
                if self.pop[pos] == None or self.pop[pos][Base.ID_FIT] > larvae_fit:
                    self.pop[pos] = [larvae, larvae_fit]
                    break

    def asexual_reproduction_and_depredation(self):
        self.num_occupied = sum(np.where(self.pop != None)[0])
        num_duplicate = int(self.num_occupied * self.Fa)
        pop_real = []
        for i in range(self.pop_size):
            if self.pop[i] != None:
                pop_real.append(self.pop[i])
        pop_sorted = sorted(pop_real, key=lambda reef: reef[Base.ID_FIT])
        reefs = pop_sorted[:num_duplicate]
        ## Place larvaes
        for reef in reefs:
            for i in range(self.k):
                pos = np.random.randint(0, self.pop_size)
                if self.pop[pos] == None or self.pop[pos][Base.ID_FIT] > reef[Base.ID_FIT]:
                    self.pop[pos] = reef
                    break

        self.num_occupied = sum(np.where(self.pop != None)[0])
        num_depredation = int(self.num_occupied * self.Fd)
        if np.random.uniform() < self.Pd:
            pop_sorted = sorted(self.find_real_pop(), key=lambda element: element[0][Base.ID_FIT])
            reefs = pop_sorted[len(pop_sorted)-num_depredation:]
            for reef in reefs:
                self.pop[reef[1]] = None

    ### Crossover
    def crossover_one_child_multi_point(self, dad=None, mom=None):
        r = np.random.choice(range(0, len(dad)), 2, replace=False)
        a, b = min(r), max(r)
        return np.concatenate((dad[:a], mom[a:b], dad[b:]), axis=0)

    ### Mutation
    def mutation_swap(self, parent=None):
        r = np.random.choice(range(0, len(parent)), 2, replace=False)
        w = deepcopy(parent)
        w[r[0]], w[r[1]] = w[r[1]], w[r[0]]
        return w

    def mutation_flip_point(self, parent=None):
        w = deepcopy(parent)
        w[np.random.randint(0, len(parent))] = np.random.uniform(self.domain_range[0], self.domain_range[1])
        return w


    ### Find the real population
    def find_real_pop(self):
        pop_real = []
        for i in range(self.pop_size):
            if self.pop[i] != None:
                pop_real.append((self.pop[i], i))
        return pop_real


    def train(self):
        best_train = [None, 100000]
        self.create_population()

        for j in range(0, self.epoch):
            # Next generations
            self.broadcast_spawning_and_brooding()
            self.asexual_reproduction_and_depredation()
            if self.Pd <= self.Pd_thres:
                self.Pd += self.alpha

            # Find best reef
            pop_sorted = sorted(self.find_real_pop(), key=lambda reef: reef[Base.ID_FIT])
            best_reef_train = deepcopy(self.pop[ pop_sorted[Base.ID_REEF_AFTER_SORTED][1] ])
            if best_reef_train[1] < best_train[1]:
                best_train = best_reef_train
            print("> Epoch {}: Best training fitness {}".format(j + 1, best_train[1]))
            self.loss_train.append(best_train[1])

        return best_train[0], self.loss_train


cro_paras = {
    "epoch": 2000, "pop_size": 100,"po": 0.55,"Fb": 0.7,"Fa": 0.1,"Fd": 0.1,"Pd": 0.1,"k": 10,
    "problem_size": 50, "domain_range": (-1, 1)           # lower and upper bound
}

cro = Base(cro_para=cro_paras)
start = time.clock()
best, loss = cro.train()
CRO_time = time.clock() - start
print(best)
print(CRO_time/2000)
