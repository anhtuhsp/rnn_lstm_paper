import torch
print(torch.__version__)

tensor_arr = torch.Tensor([[1, 2], [3, 4]])
print(tensor_arr)

tensor_uninitial = torch.Tensor(3, 3)
print(tensor_uninitial.numel())      # Count number of elements

tensor_initial = torch.rand(2, 3)
print(tensor_initial)

tensor_init = torch.randn(4, 2).type(torch.IntTensor)
print(tensor_init)

tensor_long = torch.LongTensor([1.0, 2.0, 3.0])
print(tensor_long)

tensor_byte = torch.ByteTensor([0, 261, 1, -51])    # Value: 0, 255
print(tensor_byte)

tensor_ones = torch.ones(10)
tensor_zeros = torch.zeros(5)
tensor_eye = torch.eye(3)

print(tensor_ones)
print(tensor_zeros)
print(tensor_eye)

non_zero = torch.nonzero(tensor_eye)
print(non_zero)

tensor_ones_shape_eye = torch.ones_like(tensor_eye)
print(tensor_ones_shape_eye)

### 2 operation supported include: In-place (modified the original), Out-place (create new tensor)
## In-place operator with: _
init_tensor = torch.rand(3, 3)
print(init_tensor)
init_tensor.fill_(6)            # Replace original (Method In-place, don't have out-place)
print(init_tensor)

new_tensor = init_tensor.add(4)
print(new_tensor)





